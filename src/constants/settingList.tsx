export const settingList = [
  {
    isElectron: false,
    title: "Turn on touch screen mode",
    propName: "isTouch",
  },
  {
    isElectron: false,
    title: "Auto open latest book",
    propName: "isOpenBook",
  },
  {
    isElectron: true,
    title: "Remember window's size from last read",
    propName: "isRememberSize",
  },
  {
    isElectron: false,
    title: "Default expand all content",
    propName: "isExpandContent",
  },
];
export const langList = [
  { label: "简体中文", value: "zh" },
  { label: "繁體中文", value: "cht" },
  { label: "English", value: "en" },
  { label: "русский", value: "ru" },
];
export const searchList = [
  { label: "Google", value: "google" },
  { label: "百度", value: "baidu" },
  { label: "Bing", value: "bing" },
  { label: "DuckDuckGo", value: "duckduckgo" },
];
