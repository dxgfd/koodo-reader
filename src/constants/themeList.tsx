export const backgroundList = [
  "rgba(197, 231, 207,1)",
  "rgba(233, 216, 188,1)",
  "rgba(255,255,255,1)",
  "rgba(44,47,49,1)",
];
export const textList = [
  "rgba(0,0,0,1)",
  "rgba(255,255,255,1)",
  "rgba(89, 68, 41,1)",
  "rgba(54, 80, 62,1)",
];
